function LinkedList() {
  let Node = function(element)  {
    this.element = element;
    this.next = null;
  };

  let length = 0;
  let head = null;

  this.append = element => {
    // Adiciona um elemento no final da lista
    let node = new Node(element);
    let current;

    if (head == null) {
      heade = node;
    } else {
      current = head;

      while (current.next) {
        current = current.next;
      }

      current.next = node;
    }
    length++;
  };

  this.insert = (position, element) => {
    // Adiciona um elemento em uma posição específica
  };

  this.removeAt = position => {
    // remove o elemento de uma posição específica
  };

  this.remove = element => {
    // Remove o elemento element
  };

  this.indexOf = element => {
    // Retorna a posição do elemento
  };

  this.isEmpty = () => {
    // Retorna se está vazia ou não a instancia
  };

  this.size = () => {
    // retorna o tamanho da instancia
  };

  this.toString = () => {
    // Converte em uma string
    let current = head;
    let string = "";

    while (current) {
      string += current.element + " ";
      current = current.next;
    }
    return string;
  };

  this.print = () => {
    // Imprime no console
    console.log(this.toString());
  };
}

let listaLigada = new LinkedList();
listaLigada.append('Pedro')
listaLigada.append('Maria')
listaLigada.append('José')

listaLigada.print()
