function Queue() {
  let items = [];

  this.enqueue = element => {
    items.push(element);
  };

  this.dequeue = () => {
    return items.shift();
  };

  this.size = () => {
    return items.length;
  };
}

function HotPopato(namelist, num) {
  let queue = new Queue();

  for (let i = 0; i < num; i++) {
      queue.enqueue(namelist[i]);
  }

  let eliminated = "";

  while (queue.size() > 1) {
    for (let i = 0; i < num; i++) {
      queue.enqueue(queue.dequeue());
    }
    eliminated = queue.dequeue();
    console.log(`${eliminated} was eliminated from the Hot Potato game`);
  }

  return queue.dequeue();
}

let names = ["Pedro", "Maria", "Lucas", "José", "Clara", "Roberta", "Laise"];
let winner = HotPopato(names, 7);
console.log(`The Winner is ${winner}`);
