function Queue() {
  let items = [];

  this.enqueue = element => {
    // Adiciona um novo item
    items.push(element);
  };

  this.dequeue = () => {
    // Remove um item
    return items.shift();
  };

  this.front = () => {
    // Retorna o primeiro elemento da fila
    return items[0];
  };

  this.isEmpty = () => {
    // Verifica se a fila está vazia
    return items.length === 0;
  };

  this.size = () => {
    // Retorna o tamanho da fila
    return items.length;
  };

  this.print = () => {
    // Imprime a fila no console.
    console.log(items.toString());
  };
}

let fila = new Queue();
console.log(fila.isEmpty());
fila.enqueue("Carlos");
fila.enqueue("Pedro");
fila.enqueue("José");
console.log(fila.isEmpty());

fila.print();

fila.dequeue();

fila.print();
console.log(fila.size());
