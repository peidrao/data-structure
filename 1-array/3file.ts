let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

// removendo elementos a partir da posição 0 até 3 (1, 2)
numbers.splice(0, 2);
console.log(numbers);

// Adiciona números a partir da posição 0, (serão colocadados o número 1 e 2). 
// O segundo 0 indica que será adicionado elementos
numbers.splice(0, 0, 1, 2)
console.log(numbers)
