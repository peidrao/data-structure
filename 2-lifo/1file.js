function Stack() {
  let items = [];

  this.push = element => {
    // Adiciona um novo item a pilha
    items.push(element);
  };

  this.pop = () => {
    // remover o item do topo da pilha
    return items.pop();
  };

  this.peek = () => {
    // Devolve o elemento que está no topo da pilha
    return items[items.length - 1];
  };

  this.isEmpty = () => {
    // informar se a pilha está vazia
    return items.length === 0;
  };

  this.clear = () => {
    // Limpa a filha
    items = [];
  };

  this.size = () => {
    // Informa o tamanho da pilha
    return items.length;
  };

  this.print = () => {
    // Imprimir a pilha no consolo
    console.log(items.toString());
  };
}

const pilha = new Stack();

pilha.push(2);
pilha.push(4);
pilha.push(6);
pilha.push(8);
pilha.push(10);

// pilha.clear();
console.log(pilha.size());


pilha.print();
