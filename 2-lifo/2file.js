// Conversor de número decimal para binário

function decimalToBinary(number) {
  let stack = [],
    rest,
    binaryString = '';

  while (number > 0) {
    rest = Math.floor(number % 2);
    stack.push(rest);
    number = Math.floor(number / 2);
  }

  while (stack.length > 0) {
    binaryString += stack.pop().toString();
  }

  return binaryString;
}

console.log(decimalToBinary(25));
