// Decimal para qualquer base;

function baseConverter(number, base) {
  let stack = [],
    rest,
    baseString = "",
    digitos = "0123456789ABCDEF";

  while (number > 0) {
    rest = Math.floor(number % base);
    stack.push(rest);
    number = Math.floor(number / base);
  }

  while (stack.length) {
    baseString += digitos[stack.pop()];
  }

  return baseString;
}

console.log(baseConverter(123, 16))
