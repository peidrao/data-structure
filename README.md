# Estrutura de Dados

Estudo sobre as principais estruturas de dados usando Typescript

## Estrutura

1. Array
2. LIFO (Last in First Out) -> Pilha (O último a entrar é o primeiro a sair)
3. FIFO (First In First Out) -> Fila (Primeiro a entrar é o primeiro a sair)
    1. 1file.js - Implementação
    2. 2file.js - Fila Prioritária
    3. 3file.js - Fila Círcular
4. Linked List (Lista ligada)
    1. 1file.js - Implementação(esqueleto)
